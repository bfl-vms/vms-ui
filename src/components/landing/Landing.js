import React, { Component } from 'react'
import store from '../../store/store'
import { onAction } from '../../store/actions/Action'
import { connect } from "react-redux";
import "../../assets/css/icomoon.scss"

export class Landing extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             value :false
        }
    }
    
    componentDidMount(){
        store.dispatch(onAction("hii"))
    }
    componentWillReceiveProps(nextProps){
        this.setState({
            value : nextProps.commonReducer.action
        })
    }
    render() {
        console.log(this.state.value)
        return (
            <div>
                hello {this.state.value}<span class="icon-pencil"></span>   
            </div>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        commonReducer: state.commonReducer
    }
}
export default connect(mapStateToProps)(Landing);
