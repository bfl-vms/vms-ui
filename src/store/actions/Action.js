
import ON_ACTION from "../constants/actionType"

export function onAction(value){
    return {
        type : ON_ACTION,
        value : value
    }
}